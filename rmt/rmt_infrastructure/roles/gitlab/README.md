Role Name
=========

A brief description of the role goes here.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).




# Default certificates
## docker settings
#gitlab_image: 'gitlab/gitlab-ce:latest'
#hostname: 'gitlab'
#domainname: 'rmt'
#dns_servers:
#  - 172.24.26.245
#  - 172.24.26.254
#container_name: 'gitlab'
#
## gitlab settings
#external_url: 'https://gitlab.rmt'
#registry_external_url: 'https://gitlab-registry.rmt'
#ssl_cert_path: '/etc/gitlab/ssl/gitlab.rmt.crt'
#ssl_key_path: '/etc/gitlab/ssl/gitlab.rmt.key'
#registry_ssl_cert_path: '/etc/gitlab/ssl/gitlab-registry.rmt.crt'
#registry_ssl_key_path: '/etc/gitlab/ssl/gitlab-registry.rmt.key'
#registry_api_url: 'https://gitlab-registry.rmt'
#registry_host: 'gitlab-registry.rmt'
#
## ldap settings
#ldap_host: 'dc1.redmoose.rmt'
#bind_dn: 'CN=gitlab-admin,CN=Users,DC=domain,DC=rmt'
#bind_password: "{{ lookup('ENV', GITLAB_PASS) }}"
#ldap_tls_ca_path: '/etc/gitlab/ssl/root_ca.crt'
#ldap_gitlab_user_cert_content: |
#  BEGIN CERTIFICATE
#ldap_gitlab_user_key_content: |
#  BEGIN PRIVATE KEY
#ldap_base: 'CN=Domain Users,CN=Users,DC=domain,DC=rmt'
#user_filter: ''
