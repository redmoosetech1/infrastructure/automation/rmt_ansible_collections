# Ansible Collection - rmt.rmt_infrastructure

Documentation for the collection.



# RMT Ansible Collections
----
This is the centralized repository for RMT Custom Plugins, Modules and Roles.


# Introduction
----
What we have here is a rmt namespace with rmt_infrastructure collection.


# Installation
You can install from gitlab using the below command.

```bash
# ansible-galaxy collection install git@gitlab.rmt:
```



# References
- https://docs.ansible.com/ansible/latest/user_guide/collections_using.html#installing-a-collection-from-a-git-repository
