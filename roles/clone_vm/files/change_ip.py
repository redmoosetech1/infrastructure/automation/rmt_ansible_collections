#!/usr/bin/env python3
import subprocess
import sys


def write_file(net_int_data):
    with open("/etc/network/interfaces", 'w') as fp:
        fp.write(net_int_data)


def execute_cmd(cmd):
    if not isinstance(cmd, list):
        if isinstance(cmd, str):
            cmd = cmd.split(' ')
        else:
            print("[!] {cmd}")
            exit(1)

    process = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    
    return stdout.decode('utf8')


def replace_network_interfaces(dev, new_ip, gateway, cidr, dns, domain):
    net_int_file = f"""
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug {dev}
auto {dev}
iface {dev} inet static
    address {new_ip}/{cidr}
    gateway {gateway}
    dns-nameservers {dns}
    dns-search {domain}
"""
    write_file(net_int_file)
    print(net_int_file)


def restart_networking():
    execute_cmd('/etc/init.d/networking stop')
    execute_cmd('/etc/init.d/networking start')


def main(new_ip, gateway, cidr, dns, domain):
    dev = None
    output = execute_cmd("ls /sys/class/net")

    for word in output.split('\n'):
        if "ens" in word:
            dev = word
            break
        elif "enp" in word:
            dev = word
            break

    if dev is None:
        print("[!] Device is Empty")
        exit(1)

    replace_network_interfaces(dev, new_ip, gateway, cidr, dns, domain)
    restart_networking()


if __name__ == '__main__':
    main(sys.argv[1],  # new ip
         sys.argv[2],  # gateway
         sys.argv[3],  # cidr
         sys.argv[4],  # dns
         sys.argv[5])  # domain
